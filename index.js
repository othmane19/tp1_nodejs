const express = require('express')
const equipes = require('./equipes.json')
const joueurs = require('./joueurs.json');
const app = express()
app.use(express.json())
app.listen(82,()=>{
    console.log('rest api')
})

app.get('/equipes',(req,res)=>{
    res.status(200).json(equipes)
})
app.get('/equipes/:id',(req,res)=>{
    const id = parseInt(req.params.id)
    const equipe = equipes.find(e =>e.id == id)
    res.status(200).json(equipe)
})

app.post('/equipes',(req,res)=>{
    equipes.push(req.body)
    res.status(200).json(equipes)
})

app.put('/equipes/:id',(req,res)=>{
    const id = parseInt(req.params.id)
    let equipe = equipes.find(e=>e.id == id)
    equipe.name = req.body.name
    equipe.country = req.body.country
    res.status(200).json(equipe)
})

app.delete('/equipes/:id',(req,res)=>{
    const id = parseInt(req.params.id)
    let equipe = equipes.find(e=>e.id == id)
    equipes.splice(equipes.indexOf(equipe),1)
    res.status(200).json(equipes)
})

app.get('/joueurs', (req, res) => {
    res.status(200).json(joueurs);
});

app.get('/joueurs/:id', (req, res) => {
    const id = parseInt(req.params.id);
    const joueur = joueurs.find(joueur => joueur.id === id);
    if (joueur) {
        res.status(200).json(joueur);
    } else {
        res.status(404).send("Joueur non trouvé.");
    }
});

app.post('/joueurs', (req, res) => {
    const nouveauJoueur = req.body;
    joueurs.push(nouveauJoueur);
    res.status(201).json(joueurs);
});

app.put('/joueurs/:id', (req, res) => {
    const id = parseInt(req.params.id);
    const joueur = joueurs.find(joueur => joueur.id === id);
    if (joueur) {
        joueur.nom = req.body.nom;
        joueur.numero = req.body.numero;
        joueur.poste = req.body.poste;
        res.status(200).json(joueur);
    } else {
        res.status(404).send("Joueur non trouvé.");
    }
});

app.delete('/joueurs/:id', (req, res) => {
    const id = parseInt(req.params.id);
    const index = joueurs.findIndex(joueur => joueur.id === id);
    if (index !== -1) {
        joueurs.splice(index, 1);
        res.status(200).json(joueurs);
    } else {
        res.status(404).send("Joueur non trouvé.");
    }
});

app.get('/equipes/:id/joueurs', (req, res) => {
    const equipeId = parseInt(req.params.id)
    const joueursDeLEquipe = joueurs.filter(joueur => joueur.idEquipe === equipeId)
    res.status(200).json(joueursDeLEquipe)
})

app.get('/joueurs/:id/equipe',(req,res)=>{
    const id = parseInt(req.params.id)
    const joueur = joueurs.find(j=>j.id == id)
    const equipeDeJoueur = equipes.find(e=>e.id == joueur.idEquipe)
    res.status(200).json(equipeDeJoueur)
})

app.post('/joueurs/search', (req, res) => {
    const nomJoueur = req.body.name.toLowerCase();
    const joueur = joueurs.find(joueur => joueur.nom.toLowerCase().includes(nomJoueur));
    if (joueur) {
        res.status(200).json(joueur);
    } else {
        res.status(404).send("Joueur non trouvé.");
    }
});